from sys import argv
from nltk.corpus import stopwords
import math
import operator

tweetsFile = open(argv[1],"r")
tf = {} #dictionary of key=word and value=tf of that word
totalWordsInDoc = 0
stopWords = stopwords.words('english') #stopwords from nltk corpus

#Calculate tf of each word
for line in tweetsFile:           
    words = line.strip().split()   
       
    for word in words:                 
        word = word.strip().lower()
                   
        totalWordsInDoc += 1           
            
        if(word in stopWords or word[0]=='#'): #ignore stop-words        
            continue
        elif word not in tf:
            tf[word] = 1
        else:
            tf[word] += 1
            
tweetsFile.close()
        
tweetsFile = open(argv[1],"r")

idf = {}
totalTweets = 0

#calculate idf for each word
for line in tweetsFile:
    
    totalTweets += 1
    
    words = line.strip().split()
    
    uniqueWords = {}
    
    for word in words:
        
        word = word.strip().lower()
        
        if word not in uniqueWords:
            uniqueWords[word] = 1
            
    for key in uniqueWords:
        
        if key not in idf:
            idf[key] = 1
        else:
            idf[key] += 1
            
tweetsFile.close()    
    
line_no = 1
tweetWeights ={}

tweetsFile = open(argv[1],"r")

#Assign weights to tweets
for line in tweetsFile:
    
    words = line.strip().split()
    tweetWeight = 0
    
    for word in words:
        
        word = word.strip().lower()  
        
        if(word in stopWords or word[0]=='#'): #ignore stop-words        
            continue
        
        tf_current = tf[word]/totalWordsInDoc
        idf_current = totalTweets/idf[word]
    
        tweetWeight += (tf_current * math.log2(idf_current))
        
    tweetWeights[line_no] = tweetWeight #/ max(10, len(words)) #normalization factor
    
    line_no += 1
    
tweetsFile.close()        

sorted_tweets = sorted(tweetWeights.items(), key=operator.itemgetter(1), reverse=True)

for i in sorted_tweets:
    print("{} {}".format(i[0],i[1]))









