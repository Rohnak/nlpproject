from sys import argv
import sys
import linecache
import math
tif_file = open(argv[1],"r")
tweets_file = argv[2]
top_tweets_file = open(argv[3],"w")
tweets_required = int(argv[4])
threshold = float(argv[5])
count = 0
prev_tweet = ""
for line in tif_file:
    dictionary = {}
    data = line.split(" ");
    line_no = int(data[0])
    line_weight = float(data[1])
    if count == 0:
        count += 1
        tweet = linecache.getline(tweets_file,line_no)
        prev_tweet = tweet
        top_tweets_file.write(tweet)
        continue
    tweet = linecache.getline(tweets_file,line_no)
    tweet_words = tweet.split()
    for word in tweet_words:
        word = word.lower()
        if word in dictionary:
            dictionary[word][0] += 1
        else:
            dictionary[word] = [1,0]
    tweet_words = prev_tweet.split()
    for word in tweet_words:
        word = word.lower()
        if word in dictionary:
            dictionary[word][1] += 1
        else:
            dictionary[word] = [0,1]
    numerator = 0
    denominator = 0
    denominator1 = 0
    denominator2 = 0
    for i in dictionary:
        numerator += (dictionary[i][0]*dictionary[i][1])
        denominator1 += (dictionary[i][0]*dictionary[i][0])
        denominator2 += (dictionary[i][1]*dictionary[i][1])
    denominator1 = float(math.sqrt(denominator1))
    denominator2 = float(math.sqrt(denominator2))
    denominator = denominator1*denominator2
    cosine_similarity = numerator/denominator
    if(cosine_similarity >= threshold):
        continue
    top_tweets_file.write(tweet)
    count += 1
    prev_tweet = tweet
    if count == tweets_required:
        break        
top_tweets_file.close()
tif_file.close()
