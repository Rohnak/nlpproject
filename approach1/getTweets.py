from TwitterSearch import TwitterSearchOrder, TwitterSearch, TwitterSearchException
import argparse
import sys

#import urllib.request
#data = urllib.request.urlopen("", data, timeout)

def shouldAvoidURL(url):
    URLsToSkip = [
                  "youtube.com",
                  "youtu.be",
                  "facebook.com",
                  "twitter.com",
                  "instagram.com"
                 ];
                 
    for link in URLsToSkip:
        if(link in url):
            return True
    
    return False

def retrieveTweets(hashtag):
    try:
        tso = TwitterSearchOrder()

        tso.set_keywords(['#'+hashtag+"+exclude:retweets"])       

        tso.set_language("en")   
        
        tso.set_count(10)    
         
        #Authentication tokens required by the Twitter Search API
        '''ts = TwitterSearch(
                consumer_key = 'jt7DraePc5TMqfSz27DHvAnuV',
                consumer_secret = 'K8QJsCnM0D1iTNnecNbIlKv0CDrAufGgOriAFDO42AHu8t3MPd',
                access_token = '1024742023-ZS5rc6cAU5AdgVdSu5OaKeLeWxtftBTn5vzRycx',
                access_token_secret = 'ZZu1e47t7dKmeHksMF34bzzSofUyhsA8rzIVbJ5qiIVJE'
                
                consumer_key = 'pqZ1s2gCRWJSQHQvQEqMrePvL',
                consumer_secret = 'F3zhJ84xafEKhuBsCxrurwHH9unm4u59GNBCdsXhNHEpSpxoHk',
                access_token = '2271396200-sVI8NZtRuK2rXkKvd0NA3EY1VNcuAK3w57eBIcy',
                access_token_secret = 'KgyMPEpbaQL9CQfaeodDamNoZu9IazbLtYei2PmjNGbAK'
                
                consumer_key = 'gA0p8Cc8ZTVlFAyNBtHq7nHTv',
                consumer_secret = 'aqd5ULyljs4UoxjU6WmjnORojawarV8p67no9S5Jt2bS2M8lLf',
                access_token = '3149221213-t0RKPj2rpzW1gIhm6NMGzCiGy8didaeRaXAhwjR',
                access_token_secret = 'Uy10un9tOyK0gnGoqjS6Oa33dgTcp5VSayMTXT9AL2I7t'
            )'''
        
        ts = TwitterSearch(
                consumer_key = 'jt7DraePc5TMqfSz27DHvAnuV',
                consumer_secret = 'K8QJsCnM0D1iTNnecNbIlKv0CDrAufGgOriAFDO42AHu8t3MPd',
                access_token = '1024742023-ZS5rc6cAU5AdgVdSu5OaKeLeWxtftBTn5vzRycx',
                access_token_secret = 'ZZu1e47t7dKmeHksMF34bzzSofUyhsA8rzIVbJ5qiIVJE'
            )   
        
    
        #iterate over results
        urlFile = open("urls", mode="w")
        for tweet in ts.search_tweets_iterable(tso):
            
            actualTweet = ""
            for word in tweet['text'].strip().split():
                actualTweet += word.strip() + " "
                
            try:
                print("{} ".format(actualTweet), end="")
                
                for lists in tweet['entities']['urls']:
                    print("{} ".format(lists['expanded_url'].strip()), end="")
                    
                    if(shouldAvoidURL(lists['expanded_url'].strip()) == False):
                        urlFile.write(lists['expanded_url'].strip()+"\n")          
            except:
                pass #tweet['user']['screen_name'].strip(),
            
            print() #leave a line
        urlFile.close()
            
    except TwitterSearchException as e: #handle any errors
        print(e)


def main(): 
    
    #handle commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("hashtag", help="The hashtag to be used for retrieving tweets.")
    
    args = parser.parse_args()
    
    retrieveTweets(args.hashtag)
    

if __name__ == '__main__' : main()
