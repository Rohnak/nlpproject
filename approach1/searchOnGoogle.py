import argparse
import json
import urllib.request, urllib.parse
import os
import subprocess, sys

def search(filename, hashtag):
    file = open(filename, mode='r')
    
    for query in file:
        originalQuery = query.strip() + " " + hashtag.strip()
        
        urls = []
        
        #prepare query
        print("\n\nquery: {}".format(originalQuery))
        query = urllib.parse.urlencode({'q': query + hashtag})
        url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query #url = ('https://ajax.googleapis.com/ajax/services/search/web?'+'v=1.0&q='+query+'&userip=INSERT-USER-IP')
        
        try:            
            #fetch response
            search_response = urllib.request.urlopen(url)
            search_results = search_response.read().decode("utf8")
            results = json.loads(search_results)
    
            # Process the JSON string.
            print("urls returned: ")
            for i in results["responseData"]["results"]:
                urls.append(i["unescapedUrl"])
                print(i["unescapedUrl"])
                
        except BaseException as e:
            print("Error: {}".format(e))
            
        try:
            os.remove("summaries.txt")
        except:
            pass
        #write summaries of all urls to single file
        summariesFile = open("summaries.txt", mode="a")    
        for url in urls:        
            #summary = subprocess.getoutput("sumy lex-rank --length=15 --url={}".format(url))
            summary = subprocess.getoutput("python2 fetchSummaryFromURL.py {}".format(url))
            #print(summary+"\n")
            summariesFile.write(summary+"\n")
        summariesFile.close()
        
        #summary of all summaries, final output
        print("\nSummary for {}\n".format(originalQuery))
        sys.stdout.flush()
        #print(subprocess.getoutput("sumy lex-rank --length=10 --file=summaries.txt")+"\n")        
        print(subprocess.getoutput("python2 fetchSummaryFromFile.py summaries.txt"))
        sys.stdout.flush()
    
    file.close()
        
def main():
    #handle commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="The file containing the search queries.")
    parser.add_argument("--hashtag", help="The hashtag.")
    args = parser.parse_args()
    '''if(sys.argv[2]):
        hashtag = sys.argv[2]
    else:
        hashtag = ""'''
    search(args.filename, args.hashtag)

if __name__ == '__main__':main()