import argparse
import operator
from collections import OrderedDict
from nltk.corpus import stopwords
import string

def generateSearchQueries(filename):
    
    file = open(filename, mode="r")
    
    stopWords = stopwords.words('english') #stopwords from nltk corpus
        
    tokenCounts = {}  
    
    for line in file:
        tweet = line.split('\t')[0]
        taglist = line.split('\t')[1]
        
        #fetch tokens and corresponding tags
        tokens = tweet.split()
        postags = taglist.split()
        
        #count tokens      
        for token in tokens:
            token = token.lower()
            if(token in tokenCounts):
                tokenCounts[token] = tokenCounts[token]+1
            else:
                tokenCounts[token] = 1
    
    #filter stopwords and punctuations
    for word in stopWords:
        tokenCounts.pop(word, None)
    for symbol in string.punctuation:
        tokenCounts.pop(symbol, None)
            
    sorted_tokens = OrderedDict(sorted(tokenCounts.items(), key=operator.itemgetter(1), reverse=True)[:10]) #top 10
    
    file.seek(0, 0)
    
    for line in file:
        tokens = line.split('\t')[0].split()
        postags = line.split('\t')[1].split()
        
        #remove punctuations
        for token in tokens:
            for symbol in string.punctuation:
                if(symbol in token):
                    tokens.remove(token)
                    break
                
        
        for i in range(0, len(tokens)):
            token = tokens[i].lower()
            if(token in sorted_tokens and (postags[i] in ['^','N', 'A', 'R', 'V'])): 
                if(i>1):
                    print(tokens[i-2]+" "+tokens[i-1]+" ", end="")
                print(tokens[i]+" ", end="")
                if(i<len(tokens)-2):
                    print(tokens[i+1]+" "+tokens[i+2], end="")
                print()    
    
    file.close()

def main():
    #handle commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("taggedFile", help="The file containing the tagged tweets.")
    
    args = parser.parse_args()
    
    generateSearchQueries(args.taggedFile)

if __name__ == '__main__':main()