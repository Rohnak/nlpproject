#!/bin/sh
echo ""
echo "**TWITTER HASHTAG SUMMARIZATION USING TF-IDF, GOOGLE APIs AND LEX-RANK**"
echo ""
echo "Please enter a hash tag:"
read tag
echo ""
echo "Fetching tweets..."
python3 getTweets.py $tag > out.txt
echo "done"
echo ""
echo "Filtering relevant tweets using tf-idf..."
python3 tf_idf.py out.txt > tweet_weights.txt
echo "done"
echo ""
echo "Removing duplicates from filtered tweets..."
python3 cosine.py tweet_weights.txt out.txt top_tweets.txt 100 0.75
echo "done"
echo ""
echo "Performing POS tagging on the tweets..."
ark-tweet-nlp-0.3.2/runTagger.sh --no-confidence top_tweets.txt > tagged.txt
echo "done"
echo ""
echo "Generating search queries..."
python3 generateSearchQueries.py tagged.txt > searchQueries.txt
python3 tf_idf.py searchQueries.txt > searchQryWeights.txt
python3 cosine.py searchQryWeights.txt searchQueries.txt top_searches.txt 5 0.50
echo "done"
echo ""
echo "Using Google APIs to search using the generated search queries..."
python3 searchOnGoogle.py top_searches.txt --hashtag=$tag
