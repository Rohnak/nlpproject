#!/bin/sh
echo "Please enter a hash tag:"
read tag
echo "Fetching tweets......................"
python3 getTweets.py $tag > out.txt
cd ark-tweet-nlp-0.3.2
echo "Postagging the tweets using TweetNLP...................."
./runTagger.sh ../out.txt > ../tagged.txt
cd ..
python format.py tagged.txt > tweet_info
echo "Extracting information..........................."
echo "Generating Summary..........................."
python summarize.py
